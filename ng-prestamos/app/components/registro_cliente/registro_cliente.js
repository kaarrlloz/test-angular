var app = angular.module('app.registrocliente', ['restangular']);

app.controller('RegistroClienteController', ['$http', '$rootScope', '$scope', 'Restangular', 'Mensajes',
    function ($http, $rootScope, $scope, Restangular, Mensajes) {

        $scope.usuario = {
            numerocedula: null,
            nombre:null,
            apellido:null,
            fechanacimiento:null
        }

        $scope.crearUsuario = function(){

            Restangular.all('crearUsuario').post($scope.usuario).then(
                function (result) {               
                    Mensajes.showMessage({ type: 0, msg: "<b>BancaTest</b> -- Usuario creado exitosamente" });

                }
            ).catch(result => {
                Mensajes.showMessage({ type: 4, msg: "<b>BancaTest</b> -- Error al crear el usuario" });

            });

            
        }
      


        $scope.cancelar = function () {
            $scope.usuario = {
                numerocedula: null,
                nombre:null,
                apellido:null,
                fechanacimiento:null
            }
        }



    }]);