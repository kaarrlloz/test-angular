(function () {
    'use strict';


    var app = angular.module('app', ['ngRoute', 'common', 'app.directives',
    'app.registrocliente', 'app.solicitudcredito']);

    app.controller('AppController', ['$rootScope', '$scope', '$http', AppController]);

    function AppController($rootScope, $scope, $http) {

        $scope.listaMenu =
            [
            {
                tituloMenu: 'Registro de Clientes',
                title: '',
                rutaMenu: 'registro'
            }
            ,
            {
                tituloMenu: 'Solicitud de creditos',
                title: '',
                rutaMenu: 'credito'
            }
            ];


        $rootScope.$on('$routeChangeSuccess', function (next, current) {
            $(window).scrollTop(0);
            $(".active").removeClass("active");
            var url = window.location;
            var urlseparada = url.href.split('/');
            var element = $('ul.nav a').filter(function () {
                if (this.href != "") {
                    var urlseparadaelemento = this.href.split('/');
                    return this.href == url || urlseparada[urlseparada.length - 1].indexOf(urlseparadaelemento[urlseparadaelemento.length - 1]) == 0;
                }

            }).addClass('active').addClass('in').parent();
            if (element.is('li')) {
                element.addClass('active');
            }
        });
    }

    app.directive('myPostRepeatDirective', function () {
        return function ($scope) {
            if ($scope.$last) {
                $('#side-menu').metisMenu();
            }
        };
    });

    app.config(['$httpProvider', '$routeProvider', function ($httpProvider, $routeProvider) {

        
        $routeProvider.
            when('/', {
                templateUrl: '../components/registro_cliente/registro_cliente.html',
                controller: 'RegistroClienteController'
            }).
            
            when('/registro', {
                templateUrl: '../components/registro_cliente/registro_cliente.html',
                controller: 'RegistroClienteController'
            }).
            when('/credito', {
                templateUrl: '../components/solicitud_credito/solicitud_credito.html',
                controller: 'SolicitudCreditoController'
            }).

            otherwise({
                redirectTo: '/login.html'
            });


    }]);



})();