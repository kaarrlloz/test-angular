'use strict'

const mongoose = require('mongoose')
const User = require('../models/user')

function crearUsuario(req, res) {

    const user = new User({
        numerocedula: req.body.numerocedula,
        nombre: req.body.nombre,
        apellido: req.body.apellido,
        fechanacimiento: req.body.fechanacimiento

    })

    user.save((err) => {
        if (err) res.status(500).send({ message: `Error al crear el usuario` })
        res.status(201).send({nombre: user.nombre })
    })


}


module.exports = {
    crearUsuario,
}