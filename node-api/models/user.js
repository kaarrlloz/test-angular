'use strict'
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const UserSchema = Schema({
    numerocedula: {type: String, unique: true, lowercase: true},
    nombre: String,
    apellido: String,
    fechanacimiento: { type: Date, default: Date.now()}
}) 




module.exports = mongoose.model('User', UserSchema)