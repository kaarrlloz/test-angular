'use strict'

const express = require('express')
const api = express.Router()

const userCtrl = require('../controllers/user')


api.post('/crearUsuario', userCtrl.crearUsuario)


module.exports = api