'use strict'


const mongoose = require('mongoose')
const app = require('./app')
const config = require('./config')

mongoose.connect(config.db, (err, res) =>{
    if(err) {
       console.log("Error al conectar a la BD") 
    } else {
        console.log("Coneccion establecida")
        app.listen(config.port, () =>{
            console.log(`API Rest SIS Planer http://localhost:${config.port}!`)
        })
    }
    
})

